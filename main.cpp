#include <iostream>
#include <fstream>
#include <chrono>
#include "list.cpp"

using std::cout;
using std::cin;
using std::endl;

void runIntProgram() {
    cout << "---Program with type \"int\"---" << endl;

    auto collection = new SinglyLinkedList<int>;

    collection->push(1);
    collection->push(5);
    collection->push(2);
    collection->push(4);
    collection->push(17);

    collection->printCollection();
    collection->sort(false);
    cout << endl << "Sorted in ascending order" << endl << endl;
    collection->printCollection();

    int searchIndex = collection->search(4);
    cout << endl << "Number 4 search result: " << searchIndex << endl;
    searchIndex = collection->search(20);
    cout << "Number 20 search result: " << searchIndex << " (not found)" << endl;

    cout << endl << "Collection size: " << collection->getSize() << endl;

    cout << endl << "First element: " << collection->getFirst()->element << endl;
    cout << "Last element: " << collection->getLast()->element << endl;

    int deletedElement = collection->pop();
    cout << endl << "Pop result: " << deletedElement << endl;
    collection->printCollection();

    deletedElement = collection->deleteByNumber(3, true);
    cout << endl << "Delete by number 3 with shift result: " << deletedElement << endl;
    collection->printCollection();

    collection->insertByNumber(451, 1, false);
    cout << endl << "Safe insert at position 1 result" << endl;
    collection->printCollection();

    collection->insertByNumber(1200, 2, true);
    cout << endl << "Insert at position 2 with overwrite result" << endl;
    collection->printCollection();
}

void runFloatProgram() {
    cout << "---Program with type \"float\" (random collection)---" << endl;

    auto collection = new SinglyLinkedList<float>;
    srand(time(NULL));

    for (int i = 0; i < rand() % 100; i++) {
        collection->push(rand() % 100 * 1.013);
    }
    cout << endl << "Randomly generated collection" << endl;
    collection->printCollection();

    collection->sort(true);
    cout << endl << "Sorted in descending order" << endl << endl;
    collection->printCollection();

    cout << endl << "Collection size: " << collection->getSize() << endl;

    float deletedElement = collection->pop();
    cout << endl << "Pop result: " << deletedElement << endl;
    collection->printCollection();

    deletedElement = collection->deleteByNumber(5, true);
    cout << endl << "Delete by number 5 with shift result: " << deletedElement << endl;
    collection->printCollection();

    collection->insertByNumber(11.11, 10, true);
    cout << endl << "Insert at position 10 with overwrite result" << endl;
    collection->printCollection();

    cout << endl << "Collection size: " << collection->getSize() << endl;
}

Bus *initFirstBus() {
    char number[] = "BS-18621";

    const int departureDaysCount = 4;
    int departureDays[departureDaysCount];
    departureDays[0] = 1;
    departureDays[1] = 3;
    departureDays[2] = 4;
    departureDays[3] = 7;

    Time *departureTime = new Time(8, 50);
    Time *arrivalTime = new Time(3, 0);

    int departureStation = 1;
    int arrivalStation = 9;

    const int intermediateStationsCount = 3;
    int intermediateStations[intermediateStationsCount];
    intermediateStations[0] = 2;
    intermediateStations[1] = 4;
    intermediateStations[2] = 5;

    Bus *firstBus = (new Bus(147))
            ->setFlightNumber(number)
            ->setDepartureDays(departureDays, departureDaysCount)
            ->setDepartureTime(departureTime)
            ->setTravelTime(arrivalTime)
            ->setDepartureStation(departureStation)
            ->setArrivalStation(arrivalStation)
            ->setIntermediateStations(intermediateStations, intermediateStationsCount);

    return firstBus;
}

Bus *initSecondBus() {
    char number[] = "AV-99124";

    const int departureDaysCount = 3;
    int departureDays[departureDaysCount];
    departureDays[0] = 2;
    departureDays[1] = 4;
    departureDays[2] = 6;

    Time *departureTime = new Time(11, 45);
    Time *arrivalTime = new Time(6, 15);

    int departureStation = 3;
    int arrivalStation = 24;

    const int intermediateStationsCount = 5;
    int intermediateStations[intermediateStationsCount];
    intermediateStations[0] = 5;
    intermediateStations[1] = 8;
    intermediateStations[2] = 14;
    intermediateStations[3] = 16;
    intermediateStations[4] = 19;

    Bus *bus = (new Bus(225))
            ->setFlightNumber(number)
            ->setDepartureDays(departureDays, departureDaysCount)
            ->setDepartureTime(departureTime)
            ->setTravelTime(arrivalTime)
            ->setDepartureStation(departureStation)
            ->setArrivalStation(arrivalStation)
            ->setIntermediateStations(intermediateStations, intermediateStationsCount);

    return bus;
}

Bus *initThirdBus() {
    char number[] = "PO-75542";

    const int departureDaysCount = 7;
    int departureDays[departureDaysCount];
    departureDays[0] = 1;
    departureDays[1] = 2;
    departureDays[2] = 3;
    departureDays[3] = 4;
    departureDays[4] = 5;
    departureDays[5] = 6;
    departureDays[6] = 7;

    Time *departureTime = new Time(18, 05);
    Time *arrivalTime = new Time(1, 25);

    int departureStation = 7;
    int arrivalStation = 9;

    const int intermediateStationsCount = 1;
    int intermediateStations[intermediateStationsCount];
    intermediateStations[0] = 8;

    Bus *bus = (new Bus(657))
            ->setFlightNumber(number)
            ->setDepartureDays(departureDays, departureDaysCount)
            ->setDepartureTime(departureTime)
            ->setTravelTime(arrivalTime)
            ->setDepartureStation(departureStation)
            ->setArrivalStation(arrivalStation)
            ->setIntermediateStations(intermediateStations, intermediateStationsCount);

    return bus;
}

Bus *initFourthBus() {
    char number[] = "ZS-35667";

    const int departureDaysCount = 1;
    int departureDays[departureDaysCount];
    departureDays[0] = 6;

    Time *departureTime = new Time(00, 10);
    Time *arrivalTime = new Time(5, 35);

    int departureStation = 2;
    int arrivalStation = 12;

    const int intermediateStationsCount = 4;
    int intermediateStations[intermediateStationsCount];
    intermediateStations[0] = 3;
    intermediateStations[1] = 6;
    intermediateStations[2] = 9;
    intermediateStations[3] = 11;

    Bus *bus = (new Bus(371))
            ->setFlightNumber(number)
            ->setDepartureDays(departureDays, departureDaysCount)
            ->setDepartureTime(departureTime)
            ->setTravelTime(arrivalTime)
            ->setDepartureStation(departureStation)
            ->setArrivalStation(arrivalStation)
            ->setIntermediateStations(intermediateStations, intermediateStationsCount);

    return bus;
}

void runClassProgram() {
    cout << "---Program with class \"Bus\"---" << endl << endl;

    Bus *firstBus = initFirstBus();
    Bus *secondBus = initSecondBus();
    Bus *thirdBus = initThirdBus();
    Bus *fourthBus = initFourthBus();

    firstBus->printActualInformation();

    cout << endl;
    secondBus->printActualInformation();

    cout << endl;
    thirdBus->printActualInformation();

    cout << endl;
    fourthBus->printActualInformation();

    auto collection = new SinglyLinkedList<Bus>;
    collection->push(firstBus);
    collection->push(secondBus);

    cout << endl;
    collection->printCollection();

    collection->insertByNumber(thirdBus, 1, false);
    cout << endl << "Safe insert at position 1 result" << endl;
    collection->printCollection();

    collection->push(fourthBus);
    cout << endl << "Pushed another object. Size: " << collection->getSize() << endl;
    collection->printCollection();

    cout << endl << "Search by address result: " << collection->search(firstBus) << endl;

    cout << endl << "Search for a bus that goes to station 9 result (indexes array): ";
    auto buses = collection->searchByStation(9);
    for (int i = 0; i < collection->getSearchResultCount(); i++) {
        cout << endl << buses[i] << " (" << collection->getByNumber(buses[i])->getFlightNumber() << ") ";
    }
    cout << endl;

    cout << endl << "Sorting in descending order by bus number" << endl;
    collection->sort(true);
    collection->printCollection();

    cout << endl << "Unload collection to binary file. Clearing up the collection" << endl;
    char name[] = "./file.bin";
    collection->unloadToFile(name, true);
    collection->printCollection();

    cout << endl << "Load collection from binary file" << endl;
    collection->loadFromFile(name);
    collection->reverse();
    collection->printCollection();

    cout << endl;
    collection->printActualData();
}

/**
 * @return long long
 */
long long getCurrentTime() {
    return duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

void startProfiling() {
    srand(time(NULL));

    int count;
    cout << "Enter the number of elements (preferably large):" << endl;
    cin >> count;

    cout << "Selected number of items: " << count << endl;

    cout << endl << "Run sort profiling on a collection of ints. Filling the collection..." << endl;
    auto intCollection = new SinglyLinkedList<int>();
    long long startTime = getCurrentTime();
    for (int i = 0; i < count; i++) {
        intCollection->push(rand() % 100);
    }
    long long endTime = getCurrentTime();
    cout << "Filling completed. Time: " << endTime - startTime << "ms" << endl;
    cout << "Collection size: " << intCollection->getSize() << endl;

    cout << endl << "Start sorting..." << endl;
    startTime = getCurrentTime();
    intCollection->sort(true);
    endTime = getCurrentTime();
    cout << "Sorting is over. Time: " << endTime - startTime << "ms" << endl;

    cout << endl << "Enter the number of elements (preferably large):" << endl;
    cin >> count;

    cout << endl << "Start profiling searching for a collection of objects. Filling the collection..." << endl;
    auto collection = new SinglyLinkedList<Bus>();
    startTime = getCurrentTime();
    for (int i = 0; i < count; i++) {
        Bus *bus = (new Bus(i))
                ->setFlightNumber("#TESTBUS")
                ->setDepartureStation(1)
                ->setArrivalStation(rand() % 100);

        collection->push(bus);
    }
    endTime = getCurrentTime();
    cout << "Filling completed. Time: " << endTime - startTime << "ms" << endl;
    cout << "Collection size: " << collection->getSize() << endl;

    cout << endl << "Search start by non-existent station number..." << endl;
    startTime = getCurrentTime();
    collection->searchByStation(5419);
    endTime = getCurrentTime();
    cout << "Searching is over. Time: " << endTime - startTime << "ms" << endl;
}

void run() {
    cout << "Hello! Select an action and enter a number:" << endl;
    cout << "Test the operation of the application with the type \"int\": 1" << endl;
    cout << "Test the operation of the application with the type \"float\": 2" << endl;
    cout << "Test the operation of the application with the class \"Bus\": 3" << endl;
    cout << "Start profiling the program: 4" << endl;
    cout << "Exit: 0" << endl;

    int action;
    cin >> action;

    switch (action) {
        case 0:
            cout << "Bye-bye =)" << endl;
            exit(0);
        case 1:
            runIntProgram();
            break;
        case 2:
            runFloatProgram();
            break;
        case 3:
            runClassProgram();
            break;
        case 4:
            startProfiling();
            break;
        default:
            cout << "There is no such action." << endl;
            exit(0);
    }
}

int main() {
    run();

    return 0;
}