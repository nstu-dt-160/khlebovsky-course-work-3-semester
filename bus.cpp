#include "bus.h"
#include <cstring>

using std::cout;
using std::endl;

Bus::Bus(int number) {
    this->busNumber = number;

    this->flightNumber = nullptr;
    this->departureDays = nullptr;
    this->intermediateStations = nullptr;

    this->departureTime = new Time(0, 0);
    this->travelTime = new Time(0, 0);

    this->departureStation = NULL;
    this->arrivalStation = NULL;

    this->departureDaysCount = 0;
    this->intermediateStationsCount = 0;
}

Time::Time(int hours, int minutes) {
    this->hours = hours;
    this->minutes = minutes;
}

Bus::~Bus() {
    this->clearData();
}

Bus *Bus::setFlightNumber(const char *number) {
    this->flightNumber = new char[strlen(number) + 1];
    strcpy(this->flightNumber, number);

    return this;
}

char *Bus::getFlightNumber() {
    return this->flightNumber;
}

Bus *Bus::setDepartureDays(const int *days, int count) {
    this->departureDaysCount = count;
    this->departureDays = new int[sizeof(int) * count];

    for (int i = 0; i < count; i++) {
        this->departureDays[i] = days[i];
    }

    return this;
}

int *Bus::getDepartureDays() {
    return this->departureDays;
}

Bus *Bus::setDepartureTime(Time *time) {
    this->departureTime = time;

    return this;
}

Time *Bus::getDepartureTime() {
    return this->departureTime;
}

Bus *Bus::setTravelTime(Time *time) {
    this->travelTime = time;

    return this;
}

Time *Bus::getTravelTime() {
    return this->travelTime;
}


Bus *Bus::setDepartureStation(int station) {
    this->departureStation = station;

    return this;
}

int Bus::getDepartureStation() {
    return this->departureStation;
}

Bus *Bus::setArrivalStation(int station) {
    this->arrivalStation = station;

    return this;
}

int Bus::getArrivalStation() {
    return this->arrivalStation;
}

Bus *Bus::setIntermediateStations(const int *stations, int count) {
    this->intermediateStationsCount = count;
    this->intermediateStations = new int[sizeof(int) * count];

    for (int i = 0; i < count; i++) {
        this->intermediateStations[i] = stations[i];
    }

    return this;
}

int *Bus::getIntermediateStations() {
    return this->intermediateStations;
}

int Bus::getIntermediateStationsCount() const {
    return this->intermediateStationsCount;
}

void Bus::printActualInformation() {
    cout << "Actual information for Bus #" << this->busNumber << endl;

    if (this->flightNumber != nullptr) {
        cout << "Flight number: " << this->flightNumber << endl;
    } else {
        cout << "Flight number: undefined" << endl;
    }

    if (this->departureDays != nullptr) {
        cout << "Departure days: ";
        for (int i = 0; i < this->departureDaysCount; i++) {
            cout << this->departureDays[i] << " ";
        }
        cout << endl;
    } else {
        cout << "Departure days: undefined" << endl;
    }

    cout << "Departure time: " << this->departureTime->hours << "h " << this->departureTime->minutes << "m" << endl;
    cout << "Travel time: " << this->travelTime->hours << "h " << this->travelTime->minutes << "m" << endl;

    cout << "Departure station: " << this->departureStation << endl;
    cout << "Arrival station: " << this->arrivalStation << endl;

    if (this->intermediateStations != nullptr) {
        cout << "Intermediate stations: ";
        for (int i = 0; i < this->intermediateStationsCount; i++) {
            cout << this->intermediateStations[i] << " ";
        }
        cout << endl;
    } else {
        cout << "Intermediate stations: undefined" << endl;
    }
}

bool Bus::operator<(Bus &bus) const {
    return this->busNumber < bus.busNumber;
}

bool Bus::operator>(Bus &bus) const {
    return this->busNumber < bus.busNumber;
}

void Bus::unloadToFile(std::fstream &thread) {
    if (!thread.is_open()) {
        throw std::runtime_error("File is not open!");
    }

    thread.write((char *) &this->busNumber, sizeof(this->busNumber));

    unsigned int size = strlen(this->flightNumber);
    thread.write((char *) &size, sizeof(size));
    thread.write(this->flightNumber, size);

    size = sizeof(int) * this->departureDaysCount;
    thread.write((char *) &size, sizeof(size));
    thread.write((char *) this->departureDays, size);

    thread.write((char *) &this->departureTime->hours, sizeof(this->departureTime->hours));
    thread.write((char *) &this->departureTime->minutes, sizeof(this->departureTime->minutes));

    thread.write((char *) &this->travelTime->hours, sizeof(this->travelTime->hours));
    thread.write((char *) &this->travelTime->minutes, sizeof(this->travelTime->minutes));

    thread.write((char *) &this->departureStation, sizeof(this->departureStation));

    thread.write((char *) &this->arrivalStation, sizeof(this->arrivalStation));

    size = sizeof(int) * this->intermediateStationsCount;
    thread.write((char *) &size, sizeof(size));
    thread.write((char *) this->intermediateStations, size);

    this->clearData();
}

void Bus::clearData() {
    delete[] this->flightNumber;
    delete[] this->departureDays;
    delete[] this->intermediateStations;

    delete this->departureTime;
    delete this->travelTime;
}