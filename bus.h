struct Time {
    int hours;
    int minutes;

    Time(int hours, int minutes);
};

class Bus {
public:
    /**
     * Default constructor
     *
     * @param number
     */
    Bus(int number);

    /**
     * Default destructor
     */
    ~Bus();

    /**
     * @param number
     *
     * @return Bus
     */
    Bus *setFlightNumber(const char *number);

    /**
     * @return char
     */
    char *getFlightNumber();

    /**
     * @param days
     * @param count
     *
     * @return Bus
     */
    Bus *setDepartureDays(const int *days, int count);

    /**
     * @return int
     */
    int *getDepartureDays();

    /**
     * @param time
     *
     * @return Bus
     */
    Bus *setDepartureTime(Time *time);

    /**
     * @return Time
     */
    Time *getDepartureTime();

    /**
     * @param time
     *
     * @return Time
     */
    Bus *setTravelTime(Time *time);

    /**
     * @return Time
     */
    Time *getTravelTime();

    /**
     * @param station
     *
     * @return Bus
     */
    Bus *setDepartureStation(int station);

    /**
     * @return int
     */
    int getDepartureStation();

    /**
     * @param station
     *
     * @return Bus
     */
    Bus *setArrivalStation(int station);

    /**
     * @return int
     */
    int getArrivalStation();

    /**
     * @param stations
     * @param count
     *
     * @return Bus
     */
    Bus *setIntermediateStations(const int *stations, int count);

    /**
     * @return int
     */
    int *getIntermediateStations();

    /**
     * @return int
     */
    [[nodiscard]] int getIntermediateStationsCount() const;

    /**
     * @return void
     */
    void printActualInformation();

    /**
     * @param bus
     *
     * @return bool
     */
    bool operator<(Bus &bus) const;


    /**
     * @param bus
     *
     * @return bool
     */
    bool operator>(Bus &bus) const;

    /**
     * @param thread
     *
     * @return void
     */
    void unloadToFile(std::fstream &thread);

    /**
     * @return void
     */
    void clearData();

private:
    int busNumber;
    char *flightNumber;
    int *departureDays;

    Time *departureTime;
    Time *travelTime;

    int departureStation;
    int arrivalStation;
    int *intermediateStations;

    int departureDaysCount;
    int intermediateStationsCount;
};
