#include "bus.cpp"

template<typename Type>
class SinglyLinkedList {
private:
    struct Node {
        Type element;
        Node *next;

        Node(Type element, Node *next);
    };

    Node *first;
    int size;

public:
    /**
     * Default constructor
     */
    SinglyLinkedList();

    /**
     * @return int
     */
    int getSize();

    /**
     * @return Node
     */
    Node *getFirst();

    /**
     * @return Node
     */
    Node *getLast();

    /**
     * @return void
     */
    void printCollection();

    /**
     * Insert at the beginning
     *
     * @param element
     *
     * @return void
     */
    void push(Type element);

    /**
     * Removing the first element
     *
     * @return Type
     */
    Type pop();

    /**
     * Get element by key (0...n-1)
     *
     * @param number
     *
     * @return Type
     */
    Type getByNumber(int number);

    /**
     * Insert element by key (0...n-1)
     *
     * @param element
     * @param number
     * @param overwrite
     */
    void insertByNumber(Type element, int number, bool overwrite);

    /**
     * Delete element by key (0...n-1)
     *
     * @param number
     * @param shift
     *
     * @return Type
     */
    Type deleteByNumber(int number, bool shift);

    /**
     * @param byAsc asc or desc
     *
     * @return void
     */
    void sort(bool byAsc);

    /**
     * @param value
     *
     * @return int
     */
    int search(Type value);

    /**
     * @return void
     */
    void reverse();
};

template<>
class SinglyLinkedList<Bus> {
private:
    struct Node {
        Bus *element;
        Node *next;

        Node(Bus *element, Node *next);
    };

    Node *first;
    int size;
    int searchResultCount;

public:
    /**
     * Default constructor
     */
    SinglyLinkedList();

    /**
     * @return int
     */
    int getSize();

    /**
     * @return Node
     */
    Node *getFirst();

    /**
     * @return Node
     */
    Node *getLast();

    /**
     * @return void
     */
    void printCollection();

    /**
     * Insert at the beginning
     *
     * @param element
     *
     * @return void
     */
    void push(Bus *element);

    /**
     * Removing the first element
     *
     * @return Type
     */
    Bus *pop();

    /**
     * Get element by key (0...n-1)
     *
     * @param number
     *
     * @return Type
     */
    Bus *getByNumber(int number);

    /**
     * Insert element by key (0...n-1)
     *
     * @param element
     * @param number
     * @param overwrite
     */
    void insertByNumber(Bus *element, int number, bool overwrite);

    /**
     * Delete element by key (0...n-1)
     *
     * @param number
     * @param shift
     *
     * @return Type
     */
    Bus *deleteByNumber(int number, bool shift);

    /**
     * @param byAsc asc or desc
     *
     * @return void
     */
    void sort(bool byAsc);

    /**
     * @param value
     *
     * @return int
     */
    int search(Bus *value);

    /**
     * Returns the numbers of elements that go to the specified station
     *
     * @param station
     *
     * @return int
     */
    int *searchByStation(int station);

    /**
     * @param filename
     * @param clear
     *
     * @return void
     */
    void unloadToFile(const char *filename, bool clear);

    /**
     * @param filename
     *
     * @return void
     */
    void loadFromFile(const char *filename);

    /**
     * @return void
     */
    void printActualData();

    /**
     * @return void
     */
    void reverse();

    /**
     * @return int
     */
    [[nodiscard]] int getSearchResultCount() const;
};
