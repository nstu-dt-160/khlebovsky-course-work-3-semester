#include "list.h"
#include <stdexcept>

using std::cout;
using std::cin;
using std::endl;

template<typename Type>
SinglyLinkedList<Type>::SinglyLinkedList() {
    this->first = nullptr;
    this->size = 0;
}

template<typename Type>
SinglyLinkedList<Type>::Node::Node(Type element, SinglyLinkedList::Node *next) {
    this->element = element;
    this->next = next;
}

template<typename Type>
int SinglyLinkedList<Type>::getSize() {
    return this->size;
}

template<typename Type>
SinglyLinkedList<Type>::Node *SinglyLinkedList<Type>::getFirst() {
    return this->first;
}

template<typename Type>
SinglyLinkedList<Type>::Node *SinglyLinkedList<Type>::getLast() {
    SinglyLinkedList::Node *head = this->first;
    while (head->next != nullptr) {
        head = head->next;
    }

    return head;
}

template<typename Type>
void SinglyLinkedList<Type>::printCollection() {
    if (this->first == nullptr) {
        cout << "Collection is empty" << endl;
    } else {
        SinglyLinkedList::Node *head = this->first;
        cout << "Collection:" << endl;
        int i = 0;
        while (head != nullptr) {
            cout << i << ": " << head->element << endl;
            head = head->next;
            i++;
        }
    }
}

template<typename Type>
void SinglyLinkedList<Type>::push(Type element) {
    auto *newNode = new Node(element, this->first);
    this->first = newNode;
    this->size++;
}

template<typename Type>
Type SinglyLinkedList<Type>::pop() {
    if (this->size == 0) {
        throw std::runtime_error("SinglyLinkedList collection is empty!");
    }

    SinglyLinkedList::Node *currentFirst = this->first;

    auto element = this->first->element;
    this->first = this->first->next;

    delete currentFirst;

    this->size--;

    return element;
}

template<typename Type>
Type SinglyLinkedList<Type>::getByNumber(int number) {
    if (this->size == 0) {
        throw std::runtime_error("SinglyLinkedList collection is empty!");
    }

    if (number + 1 > this->size || number < 0) {
        throw std::out_of_range("Element with this number does not exist!");
    }

    SinglyLinkedList::Node *head = this->first;

    if (number == 0) {
        return head->element;
    }

    int i = 0;
    while (true) {
        head = head->next;
        i++;

        if (i == number) {
            return head->element;
        }
    }
}

template<typename Type>
void SinglyLinkedList<Type>::insertByNumber(Type element, int number, bool overwrite) {
    if (number < 0) {
        throw std::out_of_range("Number cannot be negative!");
    }

    SinglyLinkedList::Node *head = this->first;

    if (number + 1 > this->size) {
        int i = 0;
        while (true) {
            head = head->next;

            i++;
            if (i == number) {
                head->element = element;
                this->size = number + 1;
                break;
            }

            if (head->next == nullptr) {
                head->next = new SinglyLinkedList::Node(NULL, nullptr);
            }
        }
    } else {
        int i = 0;
        while (true) {
            if (i == number) {
                if (overwrite) {
                    head->element = element;
                } else {
                    auto currentElement = head->element;
                    this->size++;
                    head->element = element;

                    if (number + 1 == this->size) {
                        head->next = new SinglyLinkedList::Node(currentElement, nullptr);
                    } else {
                        head->next = new SinglyLinkedList::Node(currentElement, head->next);
                    }
                }

                break;
            }

            head = head->next;
            i++;
        }
    }
}

template<typename Type>
Type SinglyLinkedList<Type>::deleteByNumber(int number, bool shift) {
    if (number + 1 > this->size || number < 0) {
        throw std::out_of_range("Element with this number does not exist!");
    }

    if (number == 0) {
        auto currentElement = this->first->element;
        if (shift) {
            this->first = this->first->next;
            this->size--;
        } else {
            this->first->element = NULL;
        }

        return currentElement;
    }

    SinglyLinkedList::Node *prev = this->first;
    SinglyLinkedList::Node *head = prev->next;
    int i = 1;

    while (true) {
        if (number == i) {
            auto currentElement = head->element;
            if (shift) {
                prev->next = head->next;
                this->size--;
            } else {
                head->element = NULL;
            }

            return currentElement;
        }

        prev = head;
        head = prev->next;
        i++;
    }
}

template<typename Type>
void SinglyLinkedList<Type>::sort(bool byAsc) {
    if (this->size == 0) {
        return;
    }

    SinglyLinkedList::Node *head = this->first, *temp;
    while (head != nullptr) {
        temp = head->next;
        while (temp != nullptr) {
            if (
                    (byAsc && head->element < temp->element)
                    || (!byAsc && head->element > temp->element)
                    ) {
                auto element = head->element;
                head->element = temp->element;
                temp->element = element;
            }

            temp = temp->next;
        }

        head = head->next;
    }
}

template<typename Type>
int SinglyLinkedList<Type>::search(Type value) {
    SinglyLinkedList::Node *head = this->first;

    int i = 0;
    do {
        if (head->element == value) {
            return i;
        }

        head = head->next;
        i++;
    } while (head != nullptr);

    return -1;
}

template<typename Type>
void SinglyLinkedList<Type>::reverse() {
    SinglyLinkedList::Node *current = this->first, *prev = nullptr, *next;

    while (current != nullptr) {
        next = current->next;

        current->next = prev;

        prev = current;
        current = next;
    }

    this->first = prev;
}

SinglyLinkedList<Bus>::SinglyLinkedList() {
    this->first = nullptr;
    this->size = 0;
    this->searchResultCount = 0;
}

SinglyLinkedList<Bus>::Node::Node(Bus *element, SinglyLinkedList::Node *next) {
    this->element = element;
    this->next = next;
}

int SinglyLinkedList<Bus>::getSize() {
    return this->size;
}

SinglyLinkedList<Bus>::Node *SinglyLinkedList<Bus>::getFirst() {
    return this->first;
}

SinglyLinkedList<Bus>::Node *SinglyLinkedList<Bus>::getLast() {
    SinglyLinkedList::Node *head = this->first;
    while (head->next != nullptr) {
        head = head->next;
    }

    return head;
}

void SinglyLinkedList<Bus>::printCollection() {
    if (this->first == nullptr) {
        cout << "Collection is empty" << endl;
    } else {
        SinglyLinkedList::Node *head = this->first;
        cout << "Collection:" << endl;
        int i = 0;
        while (head != nullptr) {
            if (head->element == nullptr) {
                cout << i << ": nullptr" << endl;
            } else {
                cout << i << ": Bus object with flight number " << head->element->getFlightNumber() << endl;
            }

            head = head->next;
            i++;
        }
    }
}

void SinglyLinkedList<Bus>::push(Bus *element) {
    auto *newNode = new Node(element, this->first);
    this->first = newNode;
    this->size++;
}

Bus *SinglyLinkedList<Bus>::pop() {
    if (this->size == 0) {
        throw std::runtime_error("SinglyLinkedList collection is empty!");
    }

    SinglyLinkedList::Node *currentFirst = this->first;

    Bus *element = this->first->element;
    this->first = this->first->next;

    delete currentFirst;

    this->size--;

    return element;
}

Bus *SinglyLinkedList<Bus>::getByNumber(int number) {
    if (this->size == 0) {
        throw std::runtime_error("SinglyLinkedList collection is empty!");
    }

    if (number + 1 > this->size || number < 0) {
        throw std::out_of_range("Element with this number does not exist!");
    }

    SinglyLinkedList::Node *head = this->first;

    if (number == 0) {
        return head->element;
    }

    int i = 0;
    while (true) {
        head = head->next;
        i++;

        if (i == number) {
            return head->element;
        }
    }
}

void SinglyLinkedList<Bus>::insertByNumber(Bus *element, int number, bool overwrite) {
    if (number < 0) {
        throw std::out_of_range("Number cannot be negative!");
    }

    SinglyLinkedList::Node *head = this->first;

    if (number + 1 > this->size) {
        int i = 0;
        while (true) {
            head = head->next;

            i++;
            if (i == number) {
                head->element = element;
                this->size = number + 1;
                break;
            }

            if (head->next == nullptr) {
                head->next = new SinglyLinkedList::Node(nullptr, nullptr);
            }
        }
    } else {
        int i = 0;
        while (true) {
            if (i == number) {
                if (overwrite) {
                    head->element = element;
                } else {
                    auto currentElement = head->element;
                    this->size++;
                    head->element = element;

                    if (number + 1 == this->size) {
                        head->next = new SinglyLinkedList::Node(currentElement, nullptr);
                    } else {
                        head->next = new SinglyLinkedList::Node(currentElement, head->next);
                    }
                }

                break;
            }

            head = head->next;
            i++;
        }
    }
}

Bus *SinglyLinkedList<Bus>::deleteByNumber(int number, bool shift) {
    if (number + 1 > this->size || number < 0) {
        throw std::out_of_range("Element with this number does not exist!");
    }

    if (number == 0) {
        auto currentElement = this->first->element;
        if (shift) {
            this->first = this->first->next;
            this->size--;
        } else {
            this->first->element = nullptr;
        }

        return currentElement;
    }

    SinglyLinkedList::Node *prev = this->first;
    SinglyLinkedList::Node *head = prev->next;
    int i = 1;

    while (true) {
        if (number == i) {
            auto currentElement = head->element;
            if (shift) {
                prev->next = head->next;
                this->size--;
            } else {
                head->element = nullptr;
            }

            return currentElement;
        }

        prev = head;
        head = prev->next;
        i++;
    }
}

void SinglyLinkedList<Bus>::sort(bool byAsc) {
    if (this->size == 0) {
        return;
    }

    SinglyLinkedList::Node *head = this->first, *temp;
    while (head != nullptr) {
        temp = head->next;
        while (temp != nullptr) {
            if (
                    (byAsc && *head->element < *temp->element)
                    || (!byAsc && *head->element > *temp->element)
                    ) {
                auto element = head->element;
                head->element = temp->element;
                temp->element = element;
            }

            temp = temp->next;
        }

        head = head->next;
    }
}

int SinglyLinkedList<Bus>::search(Bus *value) {
    SinglyLinkedList::Node *head = this->first;

    int i = 0;
    do {
        if (head->element == value) {
            return i;
        }

        head = head->next;
        i++;
    } while (head != nullptr);

    return -1;
}

int *SinglyLinkedList<Bus>::searchByStation(int station) {
    int *buses = new int[this->size];
    SinglyLinkedList::Node *head = this->first;

    this->searchResultCount = 0;
    int count = 0;
    int i = 0;
    do {
        if (head->element->getArrivalStation() == station) {
            buses[count] = i;
            count++;
        } else {
            for (int j = 0; j < head->element->getIntermediateStationsCount(); j++) {
                if (head->element->getIntermediateStations()[j] == station) {
                    buses[count] = i;
                    count++;
                    break;
                }
            }
        }

        head = head->next;
        i++;
    } while (head != nullptr);

    this->searchResultCount = count;

    return buses;
}

int SinglyLinkedList<Bus>::getSearchResultCount() const {
    return this->searchResultCount;
}

void SinglyLinkedList<Bus>::unloadToFile(const char *filename, bool clear) {
    char *path = new char[strlen(filename)];
    strcpy(path, filename);

    std::fstream file;
    file.open(
            "./file.bin",
            clear
            ? std::fstream::out | std::ios::binary | std::ofstream::trunc
            : std::fstream::out | std::ios::binary
    );

    file.write((char *) &this->size, sizeof(this->size));

    int i = 0;
    SinglyLinkedList::Node *head = this->first;
    while (head != nullptr) {
        head->element->unloadToFile(file);
        this->deleteByNumber(i, true);

        head = head->next;
    }

    file.close();
}

void SinglyLinkedList<Bus>::loadFromFile(const char *filename) {
    char *path = new char[strlen(filename)];
    strcpy(path, filename);

    std::fstream thread;
    thread.open("./file.bin", std::fstream::in | std::ios::binary);

    if (!thread.is_open()) {
        throw std::runtime_error("File is not open!");
    }

    int collectionSize;
    thread.read((char *) &collectionSize, sizeof(this->size));

    for (int i = 0; i < collectionSize; i++) {
        int number;
        thread.read((char *) &number, sizeof(int));
        Bus *bus = new Bus((int) number);

        int tempSize;
        thread.read((char *) &tempSize, sizeof(tempSize));
        char *flightNumber = new char[tempSize + 1];
        thread.read(flightNumber, tempSize);
        bus->setFlightNumber(flightNumber);

        thread.read((char *) &tempSize, sizeof(tempSize));
        int *departureDays = new int[tempSize / 4];
        thread.read((char *) departureDays, tempSize);
        bus->setDepartureDays(departureDays, tempSize / 4);

        int tempHours, tempMinutes;
        thread.read((char *) &tempHours, sizeof(tempHours));
        thread.read((char *) &tempMinutes, sizeof(tempMinutes));
        Time *departureTime = new Time(tempHours, tempMinutes);
        bus->setDepartureTime(departureTime);

        thread.read((char *) &tempHours, sizeof(tempHours));
        thread.read((char *) &tempMinutes, sizeof(tempMinutes));
        Time *travelTime = new Time(tempHours, tempMinutes);
        bus->setTravelTime(travelTime);

        int station;
        thread.read((char *) &station, sizeof(station));
        bus->setDepartureStation(station);

        thread.read((char *) &station, sizeof(station));
        bus->setArrivalStation(station);

        thread.read((char *) &tempSize, sizeof(tempSize));
        int *intermediateStations = new int[tempSize / 4];
        thread.read((char *) intermediateStations, tempSize);
        bus->setIntermediateStations(intermediateStations, tempSize / 4);

        this->push(bus);
    }
}

void SinglyLinkedList<Bus>::printActualData() {
    if (this->first == nullptr) {
        cout << "Collection is empty" << endl;
    } else {
        SinglyLinkedList::Node *head = this->first;
        cout << "Actual data for collection objects:" << endl;
        int i = 0;
        while (head != nullptr) {
            if (head->element == nullptr) {
                cout << i << ": nullptr" << endl;
            } else {
                cout << endl;
                head->element->printActualInformation();
            }

            head = head->next;
            i++;
        }
    }
}

void SinglyLinkedList<Bus>::reverse() {
    SinglyLinkedList::Node *current = this->first, *prev = nullptr, *next;

    while (current != nullptr) {
        next = current->next;

        current->next = prev;

        prev = current;
        current = next;
    }

    this->first = prev;
}